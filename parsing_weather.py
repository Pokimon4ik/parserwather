# -*- coding: utf-8 -*-
import re
import statistics
from datetime import datetime, timedelta
from pprint import pprint
from statistics import mean, mode
from typing import Dict
import requests
from bs4 import BeautifulSoup

MONTHS = {
    1: 'января',
    2: 'февраля',
    3: 'марта',
    4: 'апреля',
    5: 'мая',
    6: 'июня',
    7: 'июля',
    8: 'августа',
    9: 'сентября',
    10: 'октября',
    11: 'ноября',
    12: 'декабря'
}

URL = 'https://pogoda33.ru/погода-новосибирск/день/'


class InfoDay:

    def __init__(self, date, temperature=None, precipitation=None, pressure=None, wind_speed=None,
                 wind_direction=None, humidity=None, dew_point=None, cloudiness=None):
        self.date = date
        self.temperature = temperature
        self.precipitation = precipitation
        self.pressure = pressure
        self.wind_speed = wind_speed
        self.wind_direction = wind_direction
        self.humidity = humidity
        self.dew_point = dew_point
        self.cloudiness = cloudiness

    def print_all_param(self):
        for key, variable in self.__dict__.items():
            print(f"{key}: ", end="")
            if variable:
                print(variable)
        print('-'*20)

    def avg(self, dict=True):
        avg_info_day = InfoDay(self.date)
        for key, variable in self.__dict__.items():
            if key != "date":
                if variable:
                    if key == 'cloudiness':
                        for el in variable:
                            if "дождь" in el.lower():
                                avg_info_day.__dict__[key] = 'дождь'
                                break
                            if "снег" in el.lower():
                                avg_info_day.__dict__[key] = 'снег'
                                break
                        else:
                            try:
                                avg_info_day.__dict__[key] = mode(variable)
                            except statistics.StatisticsError as err:
                                lst_count = [x for x in set(variable) if variable.count(x) > 1]
                                avg_info_day.__dict__[key] = max(lst_count)
                            if 'облачно' or 'пасмурно' in avg_info_day.__dict__[key].lower():
                                avg_info_day.__dict__[key] = 'облачно'
                    else:
                        # Содержит числа или строку
                        if re.match(r"-?\d\.?", variable[0]):
                            # среднее арифметическое списка с округление до 2
                            avg_info_day.__dict__[key] = round(mean([float(x) for x in variable]), 2)
                        else:
                            try:
                                avg_info_day.__dict__[key] = mode(variable)
                            except statistics.StatisticsError as err:
                                lst_count = [x for x in set(variable) if variable.count(x) > 1]
                                avg_info_day.__dict__[key] = max(lst_count)
        if avg_info_day.__dict__['pressure']:
            avg_info_day.__dict__['pressure'] //= 1.333
        if dict:
            return self.class_to_dict(avg_info_day)
        else:
            return avg_info_day

    def class_to_dict(self, info_day=None):
        """

        :param info_day: изменяемый класс
        :return: словарь, значения которых взяты из атрибутов класса
        """
        cur_class = info_day or self
        out_dict = {}
        for key, variable in cur_class.__dict__.items():
            out_dict[key] = variable
        return out_dict


class WeatherMaker:
    RE_TEMP = r"([-]?\d+)°C"
    RE_PRECIPITATION = r"((\d+.\d+|0))мм"
    RE_PRESSURE = r"(\d+) гПа"
    RE_WIND_SPEED = r"(\d+) м/с"
    RE_HUMIDITY = r"(\d+)%"
    RE_DEW_POINT = r"(([-]?\d+|[-]?\d+.\d+))°C"
    RE_WIND_DIRECTION = r"(\D+)"

    def __init__(self):
        self.info_days: Dict[datetime, InfoDay] = {}

    def run(self, start_date, end_date):
        """Обработка погоды по заданным промежуткам в формате %d.%m.%Y

        :param start_date: Дата начала промежутка
        :param end_date: Дата конца промежутка
        :return: словарь [дата, иформации о дне]
        """
        cur_date = datetime.strptime(start_date, "%d.%m.%Y")
        end_date = datetime.strptime(end_date, "%d.%m.%Y")
        while cur_date <= end_date:
            self.get_info_day(cur_date)
            cur_date += timedelta(days=1)
        return self.info_days

    def get_info_day(self, date):
        url = self.date_to_url(date)
        soup = self._get_bs4(self._get_request(url).text)
        info_day = InfoDay(date.date())
        # Основная информация
        if soup.find_all("div", class_="days d-none d-lg-block"):
            info_day.cloudiness = self._get_cloudiness(soup, '1 sky-icon mtt')
            info_day.temperature = self._get_data(soup, self.RE_TEMP, '1 temperature')
            info_day.precipitation = self._get_data(soup, self.RE_PRECIPITATION, '1 precipitation')
            info_day.pressure = self._get_data(soup, self.RE_PRESSURE, '1 pressure')
            info_day.wind_speed = self._get_data(soup, self.RE_WIND_SPEED, '1 wind-speed')
            info_day.wind_direction = self._get_data(soup, self.RE_WIND_DIRECTION, '1 wind-speed')
            info_day.humidity = self._get_data(soup, self.RE_HUMIDITY, '1 dew-point')
            info_day.dew_point = self._get_data(soup, self.RE_DEW_POINT, '1 dew-point')
        # Если отсутствет основная - краткая ифнормация
        elif soup.find_all("div", class_="days shadow"):
            info_day.cloudiness = self._get_cloudiness(soup, '2 sky-icon mtt')
            info_day.temperature = self._get_data(soup, self.RE_TEMP, '2 temperature')
            info_day.precipitation = self._get_data(soup, self.RE_PRECIPITATION, '2 precipitation')
        else:
            raise WeatherException(f"Данных на день не найдено|{date.date()}")
        self.info_days[date.date()] = info_day

    @staticmethod
    def _get_data(soup, re_pattern, class_name):
        data = []
        for link in soup.find_all("div", class_="col-md-" + class_name):
            match = re.match(re_pattern, link.text)
            if match:
                data.append(match.group(1).strip())
        return data

    @staticmethod
    def _get_cloudiness(soup, class_name):
        data = []
        for link in soup.find_all("div", class_="col-md-" + class_name):
            if link['data-mtt']:
                data.append(link['data-mtt'])
        return data

    @staticmethod
    def date_to_url(date):
        url_plus = str(date.day) + '-' + MONTHS[date.month]
        return URL + url_plus

    @staticmethod
    def _get_request(url):
        return requests.get(url)

    @staticmethod
    def _get_bs4(html):
        return BeautifulSoup(html, 'html.parser')

    def print_all_info(self):
        for info in self.info_days.values():
            info.print_all_param()
            print("-" * 20)


class WeatherException(Exception):
    pass


if __name__ == '__main__':
    foo = WeatherMaker()
    s_date = datetime.now() + timedelta(days=0)
    e_date = s_date + timedelta(days=3)
    str_s_date = s_date.strftime("%d.%m.%Y")
    str_e_date = e_date.strftime("%d.%m.%Y")
    pprint(foo.run(str_s_date, str_e_date))
    foo.print_all_info()
    test_date = e_date - timedelta(days=1)
    foo.info_days[test_date.date()].avg(False).print_all_param()
    pprint(foo.info_days[test_date.date()].avg())
