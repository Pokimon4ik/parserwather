from peewee import *

USER = 'root'
PASSWORD = 'root'
DB_NAME = 'peewee_weather'

dbhandle = MySQLDatabase(
    DB_NAME,
    user=USER,
    password=PASSWORD,
    host='localhost'
)


class BaseModel(Model):
    class Meta:
        database = dbhandle


class Days(BaseModel):
    id = PrimaryKeyField(null=False)
    date = DateField(null=False, unique=True)
    temperature = FloatField(null=False)
    precipitation = FloatField(null=False)
    pressure = FloatField()
    wind_speed = FloatField()
    wind_direction = TextField()
    humidity = FloatField()
    dew_point = FloatField()
    cloudiness = TextField(null=False)

    def print_all_param(self):
        for key, variable in self.__data__.items():
            print(f"{key}: ", end="")
            if variable:
                print(variable)

    def get_param(self):
        return self.__data__

    class Meta:
        order_by = ('date',)


if __name__ == '__main__':
    try:
        dbhandle.connect()
        Days.create_table()
    except InternalError as px:
        print(str(px))
