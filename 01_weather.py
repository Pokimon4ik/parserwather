# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from typing import Dict

import numpy as np
import peewee
import cv2

from data_base_setting import Days
from parsing_weather import InfoDay, WeatherMaker, WeatherException

MENU = "1. Добавить прогнозы за диапазон дат в базу данных\n" \
       "2. Получить прогнозы за диапазон дат из базы\n" \
       "3. Создать открытку из полученных прогнозов\n" \
       "4. Вывести полученные прогнозы на консоль\n" \
       "5. Выйти"

BACKGROUND_IMAGE = r'weather_img\background.png'
SUN_PATH = r'weather_img/sun.jpg'
SNOW_PATH = r'weather_img/snow.jpg'
RAIN_PATH = r'weather_img/rain.jpg'
CLOUD_PATH = r'weather_img/cloud.jpg'


class DatabaseUpdater:
    # Получающим данные из базы данных за указанный диапазон дат.
    # Сохраняющим прогнозы в базу данных (использовать peewee)

    @staticmethod
    def get_days(str_date_start, str_date_end):
        try:
            cur_date = datetime.strptime(str_date_start, "%d.%m.%Y")
            end_date = datetime.strptime(str_date_end, "%d.%m.%Y")
            data = []
            while cur_date <= end_date:
                data.append(Days.get(Days.date == cur_date.date().strftime("%Y-%m-%d")))
                cur_date += timedelta(days=1)
            return data
        except Exception:
            print("!!!Ошибка. Данных не существует в базе!!!")

    def add_info_days(self, str_date_start, str_date_end):
        try:
            info_days: Dict[datetime, InfoDay] = WeatherMaker().run(str_date_start, str_date_end)
            for info_day in info_days.values():
                self.add_dict_info(info_day.avg())
        except peewee.IntegrityError as err:
            print("!!!Ошибка. Данные по введеным дням уже существуют в базе!!!")
        except WeatherException as exc:
            print(exc)

    @staticmethod
    def add_dict_info(info_day_dict):
        Days.create(
            date=info_day_dict['date'],
            temperature=info_day_dict['temperature'],
            precipitation=info_day_dict["precipitation"],
            pressure=info_day_dict["pressure"],
            wind_speed=info_day_dict["wind_speed"],
            wind_direction=info_day_dict["wind_direction"],
            humidity=info_day_dict["humidity"],
            dew_point=info_day_dict["dew_point"],
            cloudiness=info_day_dict["cloudiness"],
        )


class ImageMaker:
    # Снабдить его методом рисования открытки
    # (использовать OpenCV, в качестве заготовки брать lesson_016/python_snippets/external_data/probe.jpg):
    #   С текстом, состоящим из полученных данных (пригодится cv2.putText)
    #   С изображением, соответствующим типу погоды
    # (хранятся в lesson_016/python_snippets/external_data/weather_img ,но можно нарисовать/добавить свои)
    #   В качестве фона добавить градиент цвета, отражающего тип погоды
    # Солнечно - от желтого к белому
    # Дождь - от синего к белому
    # Снег - от голубого к белому
    # Облачно - от серого к белому

    def __init__(self, data):

        for i, day in enumerate(data):
            self.image = cv2.imread(BACKGROUND_IMAGE)
            self.create_card(day)
            if i == 0:
                result = self.image
            else:
                result = np.vstack([result, self.image])
        self.view_image(result)

    # Функцию для отображения изображений в окнах windows:
    def view_image(self, image):
        name_of_window = "Weather Card"
        cv2.namedWindow(name_of_window, cv2.WINDOW_NORMAL)
        cv2.imshow(name_of_window, image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def create_card(self, info_day):
        text_color = (0, 0, 0)
        text_size = 2
        pen_width = 3
        data = info_day.get_param()
        if data['cloudiness'] == 'облачно':
            self.weather_img = cv2.imread(CLOUD_PATH)
            self.gradient(1)
        elif data['cloudiness'] == 'снег':
            self.weather_img = cv2.imread(SNOW_PATH)
            self.gradient(3)
        elif data['cloudiness'] == 'дождь':
            self.weather_img = cv2.imread(RAIN_PATH)
            self.gradient(2)
        else:
            self.weather_img = cv2.imread(SUN_PATH)
            self.gradient(4)

        self.image = self.overlay_transparent(self.image, self.weather_img, x=700, y=0)

        cv2.putText(self.image, str(data['date']), (20, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, text_size, text_color, pen_width)
        cv2.putText(self.image, 'Temperature: ' + str(data['temperature']), (20, 120),
                    cv2.FONT_HERSHEY_SIMPLEX, text_size, text_color, pen_width)
        cv2.putText(self.image, 'Precipitation: ' + str(data['precipitation']), (20, 190),
                    cv2.FONT_HERSHEY_SIMPLEX, text_size, text_color, pen_width)
        if data['wind_speed']:
            cv2.putText(self.image, 'Wind: ' + str(data['wind_speed']) + " " + str(data['wind_direction']), (20, 260),
                        cv2.FONT_HERSHEY_SIMPLEX, text_size, text_color, pen_width)

    def gradient(self, color_index):
        """Рисует градиент

        :param color: 1 - grey, 2 - dark_blue, blue - 3, yellow - 4
        :return:
        """
        COLOR = [
            [100, 100, 100],
            [255, 20, 0],
            [255, 150, 0],
            [0, 200, 255],
        ]
        width = int(self.image.shape[1])
        height = int(self.image.shape[0])
        original_color = COLOR[color_index - 1]
        step_plus = height / 500
        step = 0
        for i in range(width):
            B = 255 if original_color[0] + int(step) >= 255 else original_color[0] + int(step)
            G = 255 if original_color[1] + int(step) >= 255 else original_color[1] + int(step)
            R = 255 if original_color[2] + int(step) >= 255 else original_color[2] + int(step)
            step += step_plus
            for j in range(height):
                self.image[j, i] = [B, G, R]

    def overlay_transparent(self, background, overlay, x, y):

        background_width = background.shape[1]
        background_height = background.shape[0]

        if x >= background_width or y >= background_height:
            return background

        h, w = overlay.shape[0], overlay.shape[1]

        if x + w > background_width:
            w = background_width - x
            overlay = overlay[:, :w]

        if y + h > background_height:
            h = background_height - y
            overlay = overlay[:h]

        if overlay.shape[2] < 4:
            overlay = np.concatenate(
                [
                    overlay,
                    np.ones((overlay.shape[0], overlay.shape[1], 1), dtype=overlay.dtype) * 255
                ],
                axis=2,
            )

        overlay_image = overlay[..., :3]
        mask = overlay[..., 3:] / 255.0

        background[y:y + h, x:x + w] = (1.0 - mask) * background[y:y + h, x:x + w] + mask * overlay_image

        return background


def get_date():
    print("Введите первую дату в формате dd.mm.yyyy")
    start_date_str = input()
    print("Введите вторую дату в формате dd.mm.yyyy")
    end_date_str = input()
    try:
        start_date = datetime.strptime(start_date_str, "%d.%m.%Y")
        end_date = datetime.strptime(end_date_str, "%d.%m.%Y")
        if start_date > end_date:
            raise ValueError
        return start_date_str, end_date_str
    except ValueError:
        print("!!!Ошибка входных данных. Попробуйте еще раз!!!")
        return None, None


if __name__ == '__main__':
    # Сделать программу с консольным интерфейсом, постаравшись все выполняемые действия вынести в отдельные функции.
    # Среди действий, доступных пользователю, должны быть:
    #   Добавление прогнозов за диапазон дат в базу данных
    #   Получение прогнозов за диапазон дат из базы
    #   Создание открыток из полученных прогнозов
    #   Выведение полученных прогнозов на консоль
    # При старте консольная утилита должна загружать прогнозы за прошедшую неделю.
    database = DatabaseUpdater()
    user_choice = 0
    data = None
    # Меню
    while user_choice != "5":
        print(MENU)
        user_choice = input()
        if user_choice == "1":
            start_date, end_date = get_date()
            if start_date or end_date:
                database.add_info_days(start_date, end_date)
        elif user_choice == "2":
            start_date, end_date = get_date()
            if start_date or end_date:
                data = database.get_days(start_date, end_date)
        elif user_choice == "3":
            ImageMaker(data)
        elif user_choice == "4":
            for day in data:
                day.print_all_param()
        else:
            if user_choice != "5":
                print("Введите номер команды")

# зачет!
